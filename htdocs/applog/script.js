function _openLog(evt, id) {
	document.querySelectorAll(".log-tabcontent").forEach(content => content.style.display = "none");
	document.querySelectorAll(".log-tablink").forEach((link) => {
		link.className = link.className.replace(" active", "");
  });
	document.querySelector(id).style.display = "block";
	evt.currentTarget.className += " active";
}
function _updateElement(id, html) {
	document.querySelector(id).innerHTML += html;
}
function _updateLog(key, value) {
	let id = `log-tab-${document.querySelector("#log-tablinks").childElementCount}`;
	let html = `<button id="log-btn-${id}" class="log-tablink" onclick="_openLog(event, '#${id}')">${key}</button>`;
	_updateElement("#log-tablinks", html);
	html = `<div id='${id}' class='log-tabcontent'><h3>${key}</h3>${value}</div>`;
	_updateElement("#log-tab", html);
	document.querySelector(`#log-btn-${id}`).click();
}
function updateLog(key, value) {
	if (typeof value === "string")
		_updateLog(key, value);
	else
		_updateLog(key, `<pre>${JSON.stringify(value, undefined, 4)}</pre>`);
}
document.addEventListener("DOMContentLoaded", (event) => {
	let log = document.createElement("div");
	log.style = "display: none;";
  log.id = "log-tab";
	log.innerHTML = "<h2>Application log</h2>"
			+ "<div id='log-tablinks' class='log-tablink'>"
			+   "<button id='log-btn-0' class='log-tablink' onclick='_openLog(event, \"#log-tab-0\")'>Init</button>"
			+ "</div>"
			+ "<div id='log-tab-0' class='log-tabcontent'>"
			+		"<h3>Init</h3>"
			+		"<p>Automatic mode is disabled (could be triggered if CSR is filled with content)"
			+		"<input type='button' onclick='certonly()' value='Trigger certonly' />"
			+	"</p>"
			+ "</div>";
	document.body.append(log);
	// Get the element with id="defaultOpen" and click on it
	let defaultOpen = document.querySelector('#log-btn-0');
		defaultOpen.click();
});
