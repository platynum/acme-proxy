var directory;
var replayNonces = [];
var accountKey;
var accountKeyJwk;
var account;
var orders = [];

function updateReplayNonces(xmlHttp) {
	//var headers = xmlHttp.getAllResponseHeaders();
	//updateLog('headers', JSON.stringify(headers, null, true));
	var replayNonce = xmlHttp.getResponseHeader("Replay-Nonce");
	if (replayNonce !== undefined) {
		replayNonces.push(replayNonce);
		updateLog("replayNonces", replayNonces);
	}
}
function getDirectory(url) {
	return new Promise((resolve, reject) => {
		try {
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					directory = JSON.parse(this.responseText);
					updateLog("getDirectory", directory);
					sequenceAppend("RA->>Client: directory");
					updateReplayNonces(this);
					addAction("recoverAccount", directory.newAccount);
					resolve(directory);
				}
			}
			xmlHttp.onerror = function() {
				reject(xmlHttp);
			}
			sequenceAppend("Client->>RA: getDirectory()");
			xmlHttp.open("GET", url, true);
			xmlHttp.send();
		}
		catch (error) {
			reject(error);
		}
	});
}
function newNonce() {
	return new Promise((resolve, reject) => {
		try {
			var xmlHttp = new XMLHttpRequest();
			var url = directory.newNonce;
			xmlHttp.onreadystatechange = function() {
				if (this.readyState == 4 && (this.status == 200 || this.status == 204)) {
					updateReplayNonces(xmlHttp);
					sequenceAppend("CA->>Client: nonce");
					resolve(xmlHttp);
				}
			}
			xmlHttp.onerror = function() {
				reject(xmlHttp);
			}
			sequenceAppend("Client->>CA: newNonce()");
			xmlHttp.open("HEAD", url, true);
			xmlHttp.send();
		}
		catch (error) {
			reject(error);
		}
	});
}
function getAlgorithm() {
	var name = document.querySelector("#lalgorithm").value;
	if (name.startsWith("RSA"))
	{
		var params = {
			name: name,
			modulusLength: 2048,
			publicExponent: new Uint8Array([0x01, 0x00, 0x01])
		}
	}
	else {
		var params = {
			name: "ECDSA",
			namedCurve: name
		}
	}
	params.hash = {
		name: document.querySelector("#lhash").value
	}
	return params;
}
function createJwkThumbprint(jwk) {
	var pubkeyStr;
	if (jwk.kty === "EC") {
		pubkeyStr = "{" +
									"\"crv\":\"" + jwk.crv + "\"," +
									"\"kty\":\"" + jwk.kty + "\"," +
									"\"x\":\"" + jwk.x + "\"," +
									"\"y\":\"" + jwk.y + "\"" +
								"}";
	}
	else { /* RSA */
		pubkeyStr = "{" +
									"\"e\":\"" + jwk.e + "\"," +
									"\"kty\":\"" + jwk.kty + "\"," +
									"\"n\":\"" + jwk.n + "\"" +
								"}";
	}
	updateLog("pubkeyStr", pubkeyStr);
	return crypto.subtle.digest("SHA-256", new TextEncoder().encode(pubkeyStr))
			.then((hash) => {
				var hashStr = String.fromCharCode.apply(null, new Uint8Array(hash));
				return stobu(hashStr);
			});
}

function loadAccountKey() {
	var e = document.querySelector("#lloadAccountKeyValue");
	var key = window.crypto.subtle.importKey(
			"jwk", JSON.parse(e.value),
			getAlgorithm(), true, ["sign"])
	.then((key) => {
		accountKey = {};
		accountKey.privateKey = key;
		accountKeyJwk = {};
		return window.crypto.subtle.exportKey("jwk", accountKey.privateKey);
	})
	.then((jwk) => {
		accountKeyJwk.privateKey = jwk;
		updateLog("accountKeyJwk.privateKey", accountKeyJwk.privateKey);
	});
	window.crypto.subtle.exportKey("jwk", accountKey.privateKey)
	.then((jwk) => {
		delete jwk.d;
		delete jwk.dp;
		delete jwk.dq;
		delete jwk.q;
		delete jwk.qi;
		jwk.key_ops = ["verify"];
		window.crypto.subtle.importKey("jwk", jwk, getAlgorithm(), true, ["verify"])
		.then((key) => {
			return window.crypto.subtle.exportKey("jwk", key);
		})
		.then((publicKey) => {
			accountKeyJwk.publicKey = publicKey;
			updateLog("accountKeyJwk.publicKey", accountKeyJwk.publicKey);
		});
	})
	.then((jwk) => {
		return createJwkThumbprint(jwk)
	})
	.then((thumbprint) => {
		accountKeyJwk.thumbprint = thumbprint;
		updateLog("accountKeyJwk.thumbprint", accountKeyJwk.thumbprint);
	});
}
function saveAccountKey() {
	return window.crypto.subtle.exportKey("jwk", accountKey.privateKey)
	.then((key) => {
		updateLog("accountKey", "<a href='data:text/plain;charset=utf-8,"
					+ encodeURIComponent(JSON.stringify(key))
					+ "' download='accountKey.jwk' />accountKey</a><br />"
					+ "<pre>" + JSON.stringify(key) + "</pre>");
	});
}
function generateCSRKey() {
		sequenceAppend("Client->>Client: generateCSRKey()");
		let alg = document.querySelector("#lcsrAlgorithm").value;
		let key;
		switch (alg) {
				case "RSA-2048": key = KEYUTIL.generateKeypair("RSA", 2048);				break;
				case "RSA-3072": key = KEYUTIL.generateKeypair("RSA", 3072);				break;
				case "RSA-4096": key = KEYUTIL.generateKeypair("RSA", 4096);				break;
				case "P-384":		key = KEYUTIL.generateKeypair("EC", "secp384r1"); break;
				case "P-256":		key = KEYUTIL.generateKeypair("EC", "secp256r1"); break;
		}
		let jwk = KEYUTIL.getJWKFromKey(key.prvKeyObj);
		return JSON.stringify(jwk, null, 4);
}
function generateCSR(jwk, type, value) {
		sequenceAppend("Client->>Client: generateCSR()");
		let privKey = KEYUTIL.getKey(JSON.parse(jwk));
		let san;
		switch (type) {
				case 'dns':		san = [{dns: value}];		break;
				case 'email': san = [{rfc822: value}]; break;
		}
		let hash = document.querySelector("#lcsrHash").value;
		let csr = new KJUR.asn1.csr.CertificationRequest({
				subject: {str: `/CN=${value}`},
				sbjpubkey: privKey,
				extreq: [{extname: "subjectAltName", array: san}],
				sigalg: `${hash}with${privKey.type == "EC" ? "ECDSA" : "RSA"}`,
				sbjprvkey: privKey
		});
    return csr.getPEM();
}
function createAccountKey() {
	sequenceAppend("Client->>Client: createAccountKey()");
	var params = getAlgorithm();
	updateLog("params", params);
	return window.crypto.subtle.generateKey(params, true, ["sign", "verify"])
	.then((key) => {
		accountKey = key;
		accountKeyJwk = {};
		return window.crypto.subtle.exportKey("jwk", accountKey.publicKey);
	})
	.then((jwk) => {
		accountKeyJwk.publicKey = jwk;
		updateLog("accountKeyJwk.publicKey", accountKeyJwk.publicKey);
		return window.crypto.subtle.exportKey("jwk", accountKey.privateKey);
	})
	.then((jwk) => {
		accountKeyJwk.privateKey = jwk;
		updateLog("accountKeyJwk.privateKey", accountKeyJwk.privateKey);
		return createJwkThumbprint(jwk);
	})
	.then((thumbprint) => {
		accountKeyJwk.thumbprint = thumbprint;
		updateLog("accountKeyJwk.thumbprint", accountKeyJwk.thumbprint);
		return accountKeyJwk;
	})
	.catch((error) => {
		updateLog("generateKey", error);
	});
}

function stobu(str) {
	return btoa(str)
		.replace(/\+/g, "-")
		.replace(/\//g, "_")
		.replace(/=/g, "");
}
function butos(b64) {
	var tmp = b64
			.replace(/-/g, "+")
			.replace(/_/g, "/");
	if (tmp.length % 4 != 0)
		tmp += "=";
	if (tmp.length % 4 != 0)
		tmp += "=";
	return atob(tmp);
}

function _post(url, message) {
	return new Promise((resolve, reject) => {
		var xmlHttp = new XMLHttpRequest();
		updateLog("request", "POST " + url + "<pre>" + JSON.stringify(message, undefined, 4) + "<pre>");
		var protectedMessage = stobu(JSON.stringify(message.protected));
		var payload = stobu(JSON.stringify(message.payload));
		var tbs = protectedMessage + "." + payload
		// updateLog("tbs", tbs);
		var algorithm = document.querySelector("#lalgorithm").value;
		if (!algorithm.startsWith("RSA"))
			algorithm = "ECDSA";
		var signature = window.crypto.subtle.sign(
			{ name: algorithm, hash: { name: document.querySelector("#lhash").value } },
			accountKey.privateKey,
			new TextEncoder().encode(tbs)
		)
		.then((signature) => {
			xmlHttp.onreadystatechange = function() {
				// updateLog("readyState", this.readyState)
				if (this.readyState == 4) {
					var hundred = Math.floor(this.status / 100);
					if (hundred == 4 || hundred == 5)
						displayAlert("HTTP status code: " + this.status +
							"<pre>" + this.responseText + "</pre>");
					updateReplayNonces(this);
					var response = xmlHttp.responseText;
					try {
						response = JSON.parse(xmlHttp.responseText);
					}
					catch (err) { /* nothing */ }
					//resolve(xmlHttp.response);
					//resolve(JSON.parse(xmlHttp.responseText));
					resolve(xmlHttp);
				}
			}
			xmlHttp.onerror = function(ev) {
				displayAlert("xmlHttpRequest error: " +
					"<pre>" + JSON.stringify(ev, null, true) + "</pre>");
				reject(this);
			}
			var sigStr = String.fromCharCode.apply(null, new Uint8Array(signature));
			//updateLog("sigStr", btoa(sigStr));
			xmlHttp.open("POST", url);
			xmlHttp.setRequestHeader("Content-Type", "application/jose+json");
			//updateLog("content", "<pre>" + JSON.stringify(
			//	{ protected: protected, payload: payload, signature: stobu(sigStr) }) + "</pre>");
			xmlHttp.send(JSON.stringify(
				{ protected: protectedMessage, payload: payload, signature: stobu(sigStr) }));
		});
	});
}

function post(url, message) {
	message.protected = {};
	if (accountKeyJwk.publicKey.alg)
		message.protected.alg = accountKeyJwk.publicKey.alg;
	else
		message.protected.alg = document.querySelector("#lalgorithm").value.replace("P-", "ES");
	if (account === undefined)
		message.protected.jwk = accountKeyJwk.publicKey;
	else
		message.protected.kid = account.kid;
	message.protected.nonce = replayNonces.pop();
	message.protected.url = url;

	return _post(url, message);
}
function postAsGet(url) {
	return post(url, {payload: {}});
}
function _newAccount(message) {
	sequenceAppend("Client->>RA: newAccount(pubkey)");
	return post(directory.newAccount, message)
		.then((xmlHttp) => {
			account = JSON.parse(xmlHttp.responseText);
			updateLog("newAccount", account);
			account.kid = xmlHttp.getResponseHeader("Location");
			sequenceAppend("RA-->>AD: newAccount(pubkey)");
			sequenceAppend("RA->>Client: kid=" + account.kid);
			updateLog("kid", account.kid);

			addAction("deactivateAccount", account.kid);
			addAction("getOrders", account.orders);
		});
}
function newAccount(contact, termsOfServiceAgreed) {
	const message = {payload: {}};
	message.payload.termsOfServiceAgreed = termsOfServiceAgreed;
	message.payload.contact = contact;

	return _newAccount(message);
}
function recoverAccount() {
	// Delete account (and KID)
	account = undefined;

	const message = {payload: {returnOnlyExisting: true}};
	return _newAccount(message);
}
function deactivateAccount(url) {
	var message = {payload: {status: "deactivated"}};

	sequenceAppend("Client->>RA: deactivateAccount(pubkey)");
	sequenceAppend("RA->>CA: deactivateAccount(pubkey)");
	return post(directory.newAccount, message)
		.then((xmlHttp) => {
			account = JSON.parse(xmlHttp.responseText);
			updateLog("newAccount", account);
		});
}
function getOrder(url) {
	if (url !== undefined) {
		sequenceAppend("Client->>AD: getOrder()");
		return postAsGet(url)
			.then((xmlHttp) => {
				//updateLog("order", url);
				var order = JSON.parse(xmlHttp.responseText);
				sequenceAppend("AD->>Client: order");
				updateLog("getOrder", order);
				for (let authorization of order.authorizations) {
					var idx = order.authorizations.indexOf(authorization);
					addAction("getAuthorization", authorization, order.identifiers[idx].value);
				}
				addAction("finalize", order.finalize);
			});
	}
}
function getOrders(url) {
	sequenceAppend("Client->AD: getOrders()");
	return postAsGet(url)
		.then((xmlHttp) => {
			var orders = JSON.parse(xmlHttp.responseText);
			updateLog("orders", orders);
			sequenceAppend("AD->>Client: orders");
			for (let order of orders.orders) {
				addAction("getOrder", order);
			}
			return orders;
		});
}
function authorizationStatus(authorizationUrl) {
	return newNonce()
		.then(() => {
			sequenceAppend("Client->AD: getAuthorization()");
			return postAsGet(authorizationUrl);
		})
		.then((xmlHttp) => {
			let authorization = JSON.parse(xmlHttp.responseText);
			updateLog("authorization", authorization);
			sequenceAppend("AD->>Client: authorization");
			return authorization;
		});
}
function orderStatus(orderUrl) {
	let order;
	return newNonce()
		.then(() => {
			sequenceAppend("Client->AD: getOrder()");
			return postAsGet(orderUrl);
		})
		.then((xmlHttp) => {
			order = JSON.parse(xmlHttp.responseText);
			updateLog("order", order);
			sequenceAppend("AD->>Client: order");
			let auths = [];
			for (let url of order.authorizations) {
				auths.push(authorizationStatus(url));
			}
			return Promise.all(auths);
		})
		.then((authorizations) => {
			order.authorizations = authorizations;
			return order;
		});
}
function getStatus(responseHandler) {
	newNonce()
		.then(() => {
			sequenceAppend("Client->AD: getOrders()");
			return postAsGet(account.orders);
		})
		.then((xmlHttp) => {
			var orders = JSON.parse(xmlHttp.responseText);
			updateLog("orders", orders);
			sequenceAppend("AD->>Client: orders");
			let stati = [];
			for (let url of orders.orders) {
				stati.push(orderStatus(url));
			}
			return Promise.all(stati);
		})
		.then((orders) => {
			responseHandler(orders);
		});
}
function newOrder(type) {
	let message = {payload: {}};
	message.payload.identifiers = [{
			type: document.querySelector("#ltype").value,
			value: document.querySelector("#lvalue").value
	}];
	if (document.querySelector("#lstar").checked) {
		let ar = {};
		let tmp = new Date();
		ar["start-date"] = tmp.toISOString();
		tmp.setSeconds(tmp.getSeconds() + 365 * 24 * 60 * 60 * 1000);
		ar["not-after"] = tmp.toISOString();
		ar["lifetime"] = 345600;
		ar["lifetime-adjust"] = 259200;
		message.payload["auto-renewal"] = ar;
	}
	else {
		let tmp = new Date()
		message.payload.notBefore = tmp.toISOString();
		tmp.setSeconds(tmp.getSeconds() + 365 * 24 * 60 * 60 * 1000);
		message.payload.notAfter = tmp.toISOString();
	}

	sequenceAppend(`Client->>AD: newOrder(${type})`);
	return post(directory.newOrder, message)
		.then((xmlHttp) => {
			//updateLog("readyState", xmlHttp.readyState);
			//updateLog("status", xmlHttp.status);
			var order = JSON.parse(xmlHttp.responseText);
			updateLog("newOrder", order);
			orders.push(order);
			// var headers = xmlHttp.getAllResponseHeaders();
			// updateLog("headers", JSON.stringify(headers, null, true));
			order.location = xmlHttp.getResponseHeader("Location");
			addAction("getOrder", order.location);
			sequenceAppend("AD-->>CA: order=" + order.location);
			sequenceAppend("AD-->>ID: getAuthorization(pubkey, order)");
			sequenceAppend("ID-->>AD: authorization");
			sequenceAppend("AD->>Client: order=" + order.location);
			for (let authorization of order.authorizations) {
				var idx = order.authorizations.indexOf(authorization);
				addAction("getAuthorization", authorization, order.identifiers[idx].value);
			}
			addAction("finalize", order.finalize);
			addAction("cancelOrder", order.location);
			return order;
		});
}
function cancelOrder(url) {
	var message = {payload: {status: "canceled"}};

	sequenceAppend("Client->>AD: cancelOrder()");
	sequenceAppend("AD-->>CA: cancelOrder(pubkey)");
	return post(url, message)
		.then((xmlHttp) => {
			var order = JSON.parse(xmlHttp.responseText);
			sequenceAppend("AD->>Client: order");
			updateLog("cancelOrder", order);
		});
}
function finalize(url, csr)	{
	let message = {payload: {}};
	message.payload.csr = csr
						.replace("-----BEGIN CERTIFICATE REQUEST-----", "")
						.replace("-----END CERTIFICATE REQUEST-----", "")
						.replace("/", "_")
						.replace("-", "+")
						.replace("=", "");
	sequenceAppend("Client->>CA: finalize(CSR)");
	return post(url, message)
		.then((xmlHttp) => {
			//updateLog("authorization", url);
			let order = JSON.parse(xmlHttp.responseText);
			if (order.certificate) {
				sequenceAppend("CA->>Client: certificate=" + order["certificate"]);
				addAction("certificate", order.certificate);
			}
			if (order["star-certificate"] !== undefined) {
				sequenceAppend("CA->>Client: starCertificate=" + order["star-certificate"]);
				addAction("starCertificate", order["star-certificate"]);
			}
			updateLog("finalize", order);
      return order;
		});
}
function revokeCert(pem, reason) {
	let message = {payload: {}};
	message.payload.certificate = pem
						.replace(/^-{5}.*/m, '')
						.replace('/', '_')
						.replace('-', '+')
						.replace('=', '');
	message.payload.reason = reasong;
	sequenceAppend("Client->>CA: revoke(x509)");
	return post(directory.revokeCert, message)
		.then((xmlHttp) => {
			updateLog("revokeCert", xmlHttp.responseText);
			sequenceAppend("CA->>Client: revoked");
		});
}
function certificate(url, method="certificate") {
	sequenceAppend("Client->>CA: " + method + "()");
	return postAsGet(url)
		.then((xmlHttp) => {
			if (xmlHttp.getResponseHeader("Content-Type").startsWith("application/pem-certificate-chain")) {
				sequenceAppend("CA->>Client: PEM");
			}
			else {
				sequenceAppend("CA->>Client: Error");
				updateLog("error", JSON.parse(xmlHttp.responseText));
			}
      return xmlHttp.responseText;
		});
}
function starCertificate(url) {
	return certificate(url, "star-certificate");
}
function getAuthorization(url) {
	sequenceAppend(`Client->>AD: getAuthorization(${url})`);
	return postAsGet(url)
		.then((xmlHttp) => {
			//updateLog("authorization", url);
			var authorization = JSON.parse(xmlHttp.responseText);
			updateLog("getAuthorization", authorization);
			sequenceAppend("AD->>ID: createChallenge()");
			sequenceAppend("ID->>AD: challenge");
			sequenceAppend("AD->>Client: authorization");
			addAction("getAuthorization", authorization.url);
			for (let challenge of authorization.challenges) {
				updateLog(challenge.type, challenge);
				addAction("getChallenge", challenge.url, `${challenge.type} for ${authorization.identifier.value}`);
			}
			sequenceAppend("Note over Client,ID: Solve ID challenge (PostIdent, http-01, dns-01, ...)");
			return authorization;
		});
}
function getChallenge(url, token) {
	var message = {payload: {}};
	if (token !== undefined) {
		message.payload.token = token;
		message.payload.thumbprint = accountKeyJwk.thumbprint
																						.replace(/\+/g, "-")
																						.replace(/\//g, "_")
																						.replace(/=/g, "");
	}
	sequenceAppend("Client->>RA: getChallenge()");
	sequenceAppend("RA->>ID: getChallenge()");
	sequenceAppend("ID->>RA: ok");
	return post(url, message)
		.then((xmlHttp) => {
			updateLog("challenge", url);
			var challenge = JSON.parse(xmlHttp.responseText);
			sequenceAppend("RA-->>CA: authorize(order)");
			sequenceAppend("RA->>Client: ok");
			updateLog("getChallenge", challenge);
		});
}
function authorize(authorization) {
	return newNonce()
		.then(() => {
			return getAuthorization(authorization);
		})
		.then((response) => {
			var challenges = response.challenges.map(challenge => {
				if (challenge.type === "x-admin-01") {
					return newNonce()
					.then(() => {
						return getChallenge(challenge.url, challenge.token);
					})
					.then((result) => {
						return result;
					});
				}
			});
			return Promise.all(challenges);
		});
}

function certonly(url, contact, termsOfServiceAgreed, csr) {
	return getDirectory(url)
		.then((directory) => {
			return createAccountKey();
		})
		.then((key) => {
			return newAccount(contact, termsOfServiceAgreed);
		})
		.then((account) => {
			return newOrder();
		})
		.then((order) => {
			if (!order.valid) {
				//var actions = order.authorizations.map(authorize);
				var actions = [];
				for (let x of order.authorizations)
					actions.push(authorize(x));
				return Promise.all(actions)
					.then((results) => {
						return Promise.resolve(order);
					});
			}
			else {
				return Promise.resolve(order);
			}
		})
		.then((order) => {
			return finalize(order.finalize, csr);
		})
		.then((order) => {
			if (order["certificate"])
				return certificate(order["certificate"]);
			else if (order["star-certificate"])
				return starCertificate(order["star-certificate"]);
			else
				displayAlert("missing certificate link");
		})
		.catch((error) => {
			displayAlert(`error: ${error}`);
		});
}

