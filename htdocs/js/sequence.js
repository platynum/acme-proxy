var sequenceDiagramHeader = "";
var sequenceDiagramMessages = "";
function sequenceSetup(participants) {
    const header = "sequenceDiagram\n"
                 + "participant Client as Client\n"
                 + "participant RA as Registration authority\n"
                 + "participant AD as Accounting department\n"
                 + "participant ID as Identity provider\n"
                 + "participant CA as Certificate authority\n";
    sequenceDiagramHeader = header;
    for (let participant of participants)
        sequenceDiagramHeader += `participant ${participant.code} as ${participant.name}`;
    sequenceDiagramMessages = "";
}
function sequenceAppend(message) {
    if (message !== undefined)
        sequenceDiagramMessages += `${message}\n`;

    var e = document.querySelector(".mermaid");
    e.removeAttribute("data-processed");
    e.innerHTML = `${sequenceDiagramHeader}${sequenceDiagramMessages}`;

    mermaid.init(undefined, e);
}
document.addEventListener("DOMContentLoaded", (event) => {
    mermaid.initialize({
        startOnLoad: false,
        sequence: {showSequenceNumbers: true}
    });
    sequenceSetup([]);
    let header = document.createElement("h2");
    let text = document.createTextNode("Sequence diagram");
    header.appendChild(text);
    let chart = document.createElement("div");
    chart.className = "mermaid";
    chart.id = "mermaidChart0";

    let div = document.createElement("div");
    div.style.display = "none";
    div.appendChild(header);
    div.appendChild(chart);
    let element = document.body.appendChild(div);
    element.id = "mermaid";
});

