import { Buffer } from 'node:buffer';

function binaryParser(response, callback) {
    response.setEncoding("binary");
    response.data = "";
    response.on("data", function (chunk) {
        response.data += chunk;
    });
    response.on("end", function () {
        callback(undefined, Buffer.from(response.data, "binary"));
    });
}

function toArrayBuffer(buf) {
    let ab = new ArrayBuffer(buf.length);
    let view = new Uint8Array(ab);
    for (let index=0; index<buf.length; ++index) {
        view[index] = buf[index];
    }
    return ab;
}

export default { binaryParser, toArrayBuffer };
