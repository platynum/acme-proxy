import { Buffer } from 'node:buffer';
import path from 'node:path';
import url from 'node:url';
import crypto from 'node:crypto';
import { webcrypto } from 'node:crypto';
const subtle = webcrypto.subtle;
import { inspect, TextEncoder } from 'node:util';
import fs from 'node:fs';
import console from 'node:console';
import { setTimeout } from 'node:timers';
import dns from 'node:dns';
const Resolver = dns.promises;
import os from 'node:os';
import process from 'node:process';

import readline from 'readline-sync';
import Imap from 'node-imap';
import request from 'superagent';
import prefix from 'superagent-prefix';

import encode from './encode.mjs';

async function createJwkThumbprint(jwk) {
    let pubkeyString;
    if (jwk.kty === "EC") {
        pubkeyString = "{" +
                    "\"crv\":\"" + jwk.crv + "\"," +
                    "\"kty\":\"" + jwk.kty + "\"," +
                    "\"x\":\"" + jwk.x + "\"," +
                    "\"y\":\"" + jwk.y + "\"" +
                  "}";
    }
    else if (jwk.kty == 'OKP') {
        pubkeyString = "{" +
                    "\"crv\":\"" + jwk.crv + "\"," +
                    "\"kty\":\"" + jwk.kty + "\"," +
                    "\"x\":\"" + jwk.x + "\"," +
                  "}";
    }
    else { /* RSA */
        pubkeyString = "{" +
                    "\"e\":\"" + jwk.e + "\"," +
                    "\"kty\":\"" + jwk.kty + "\"," +
                    "\"n\":\"" + jwk.n + "\"" +
                  "}";
    }
    let hash = await subtle.digest('SHA-256', new TextEncoder().encode(pubkeyString));
    let hashString = String.fromCharCode.apply(undefined, new Uint8Array(hash));
    return Buffer.from(hashString, 'binary').toString('base64url');
}

class ManualChallengeResponder {
    constructor() {
        this.type = [ 'dns-01', 'http-01' ];
        this.problems = [];
    }

    async solve(key, authorization, challenge) {
        // logger.debug(`challenge: ${inspect(challenge)}`);
        let thumbprint = await createJwkThumbprint(key.jwk);
        let response = `${challenge.token}.${thumbprint}`;
        switch (challenge.type) {
            case 'http-01':
                console.log(`Setup http://${authorization.identifier.value}/.well-known/acme-challenge/${challenge.token}`);
                console.log(`with content:'${response}'`);
                break;

            case 'dns-01':
                {
                    let hash = await subtle.digest('SHA-256', new TextEncoder().encode(response));
                    let hashString = String.fromCharCode.apply(undefined, new Uint8Array(hash));
                    let b64u = Buffer.from(hashString, 'binary').toString('base64url');

                    console.log('Setup DNS TXT record with the following content:');
                    console.log(`_acme-challenge.${authorization.identifier.value} TXT ${b64u}`);
                }
                break;
            default:
                throw new Error(`Unsupported challenge type (${challenge.type})`);
        }
        readline.question('<enter>');
        logger.info('challenge response was setup');
    }
}

class DnsChallengeResponder {
    constructor() {
        this.type = 'dns-01';
        this.problems = [];
    }
    static async createValue(key, authorization, challenge) {
        let thumbprint = await createJwkThumbprint(key.jwk);
        let response = `${challenge.token}.${thumbprint}`;
        let hash = await subtle.digest('SHA-256', new TextEncoder().encode(response));
        let hashString = String.fromCharCode.apply(undefined, new Uint8Array(hash));
        return Buffer.from(hashString, 'binary').toString('base64url');
    }
}

class DnsProxyChallengeResponder extends DnsChallengeResponder {
    constructor(database) {
        super();
        this.database = database;
    }
    async solve(key, authorization, challenge) {
        if (challenge.type !== 'dns-01')
            throw new Error(`Unsupported challenge type (${challenge.type})`);

        let b64u = await this.createValue(key, authorization, challenge);
        this.database.add({
            type: 16, class: 1, ttl: 120,
            name: `_acme-challenge.${authorization.identifier.value}`,
            data: [ `${b64u}` ]
        });
    }
}

class DnsmasqChallengeResponder extends DnsChallengeResponder {
    constructor(pid, config) {
        super();
        this.pid = pid;
        this.config = config;
    }
    async solve(key, authorization, challenge) {
        if (challenge.type !== 'dns-01')
            throw new Error(`Unsupported challenge type (${challenge.type})`);

        let b64 = await this.createValue(key, authorization, challenge);
        let domain = `_acme-challenge.${authorization.identifier.value}`;
        this.problems.push({domain, value: b64});

        let buf = '';
        for (let problem of this.problems)
            buf += `txt-record=${problem.domain},${problem.value}\n`;

        fs.writeFileSync(config, buf, 'utf8');
        process.kill(this.pid, 'SIGHUP');
    }
}

class WebrootChallengeResponder {
    constructor(webroot) {
        this.webroot = webroot
        this.type = 'http-01';
        this.problems = [];
    }
    async solve(key, authorization, challenge) {
        logger.debug(`challenge: ${inspect(challenge)}`);
        const thumbprint = await createJwkThumbprint(key.jwk);
        const response = `${challenge.token}.${thumbprint}`;
        if (challenge.type != this.type)
            throw new Error(`Cannot solve ${challenge.type} challenge`);

        const filename = path.join(this.webroot, '.well-known/acme-challenge', challenge.token);
        logger.info(`creating ${filename}`);
        fs.writeFileSync(filename, response, { encoding: 'utf8', mode: 0o644 });
    }
}

class EmailChallengeResponder {
    setupImap(imap, subjectHandler) {
        this.imap = imap;
        if (imap) {
            imap.once('error', (error) => {
                logger.error(`IMAP error: ${error}`);
                if (error) throw error;
            });
            imap.once('ready', () => {
                logger.debug('Connected, opening INBOX ...');
                imap.openBox('INBOX', false, (error, box) => {
                    if (error) throw error;
                    logger.debug(`searching INBOX with ${box.messages.new} unseen of ${box.messages.total} messages for ACME challenges ...`);
                    imap.search(['UNSEEN', ['SUBJECT', 'ACME: ']], (error, results) => {
                        logger.info(`Found ${results.length} challenge(s)`);
                        var f = imap.fetch(results, {
                            bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE DKIM-SIGNATURE AUTHENTICATION-RESULTS)', 'TEXT'],
                            markSeen: true
                        });
                        f.on('message', function(message, seqno) {
                            message.on('body', function(stream, info) {
                                if (info.which === 'TEXT')
                                    logger.debug(`fetching message ${seqno} with ${info.size} bytes ...`);
                                else
                                    logger.debug(`fetching headers of message ${seqno} (${info.size} bytes) ...`);
                                var buffer = '';
                                stream.on('data', function(chunk) {
                                    buffer += chunk.toString('utf8');
                                });
                                stream.once('end', function() {
                                    let headers = Imap.parseHeader(buffer);
                                    if (headers?.subject.length > 0 && headers.subject[0].startsWith('ACME: ')) {
                                        /* TODO: Verify S/MIME or DKIM here */
                                        if (headers['authentication-results'] && headers['authentication-results'].length > 0) {
                                            if (/dkim=pass/.test(headers['authentication-results'][0]))
                                                logger.debug(`found dkim=pass in "${headers['authentication-results'][0]}`);
                                            else
                                                logger.warn(`missing dkim=pass in "${headers['authentication-results'][0]}`);
                                            /* Check d= and throw error on permfail */
                                        }
                                        subjectHandler(undefined, {
                                            to: headers.to,
                                            token: headers.subject[0].slice(6)
                                        });
                                    }
                                });
                            });
                        });
                        f.once('end', function() {
                            imap.end();
                        });
                    });
                });
            });
        }
    }

    async sendReply(error, challenge) {
        logger.debug(`Challenge (via e-mail): ${inspect(challenge)}`);
        let part1 = Buffer.from(challenge.token, 'base64');
        //logger.debug(`Parsed token-part1 (from mail): ${part1.toString('hex')}`);
        let index = this.problems.findIndex(problem => problem.authorization.identifier.value == challenge.to);
        if (index == -1)
            throw new Error('Cannot find problem for challenge received');
        let problem = this.problems[index];
        // this.problems[index] = undefined;
        let part2 = Buffer.from(problem.challenge.token, 'base64url');
        //logger.debug(`Parsed token-part2 (challenge): ${part2.toString('hex')}`);

        let token = Buffer.concat([part1, part2]);
        let thumbprint = await createJwkThumbprint(this.key.jwk);
        let keyAuthorization = `${token.toString('base64url')}.${thumbprint}`;
        // logger.debug(`keyAuthorization: ${keyAuthorization}`);

        let hash = crypto.createHash('sha256');
        hash.update(keyAuthorization);
        let response = '-----BEGIN ACME RESPONSE-----\r\n'
                     + hash.digest('base64url') + '\r\n'
                     + '-----END ACME RESPONSE-----\r\n'

        this.smtp.sendMail({
            from: challenge.to, // problem.authorization.identifier.value?
            to: problem.challenge.from,
            subject: `Re: ACME: ${part1.toString('base64url')}`,
            html: response,
        });
        problem.solved = true;
    }

    constructor(imap, smtp) {
        this.type = 'email-reply-00';
        this.problems = [];
        this.smtp = smtp;
        this.imap = imap;
        this.setupImap(imap, (error, challenge) => { this.sendReply(error, challenge); });
    }

    solve(key, authorization, challenge, timeout=12 * 1000) {
        this.key = key;
        this.problems.push({authorization: authorization, challenge: challenge});
        setTimeout(() => {
            logger.debug('Connecting to IMAP server ...');
            this.imap.connect();
        }, timeout);
    }
}

function signMessage(key, json) {
    var protectedB64u = encode.strtob64u(JSON.stringify(json.protected));
    var payloadB64u = encode.strtob64u(JSON.stringify(json.payload));
    var tbs = protectedB64u + '.' + payloadB64u;

    var signer = crypto.createSign('sha256');
    signer.update(tbs);
    var sigBuf = signer.sign(key.privateKey);

    var sigHex = sigBuf.toString('hex');
    //logger.debug(`sigHex: ${sigHex}`);

    var rLengthHex = sigHex.slice(6, 8);
    var rLength = Number.parseInt(rLengthHex, 16);
    var r = sigHex.slice(8, 8 + (rLength * 2));
    while (r.length < 64)
        r = '0' + r;
    if (r.length > 64)
        r = r.slice(2);

    //var sLengthHex = sigHex.slice(10 + (rLength * 2), 12 + (rLength * 2));
    //var sLength = Number.parseInt(sLengthHex, 16);
    var s = sigHex.slice(12 + (rLength * 2));
    while (s.length < 64)
        s = '0' + s;
    if (s.length > 64)
        s = s.slice(2);

    //var signature = encode.strtob64u(Buffer.from(r + s, 'hex').toString('binary'));
    var signature = Buffer.from(r + s, 'hex').toString('base64url');
    var content = {
        'protected': protectedB64u,
        payload: payloadB64u,
        signature: signature
    };
    //logger.debug(`sending: ${inspect(content)}`);
    return content;
}

class Client {
    constructor(uri) {
        this.base = new url.URL(uri);
        this.prefix = prefix(`${this.base.protocol}//${this.base.hostname}:${this.base.port}`);
        this.nonces = [];
    }
    static async getServer() {
        try {
            let domain = os.hostname().replace(/^[^.]*\./, '');
            let srv = `_acme-server._tcp.${domain}`;
            let addresses = await Resolver.resolveSrv(srv);
            return `https://${addresses[0].name}:${addresses[0].port}/acme/Sub/directory`;
        }
        catch { /* nothing */ }
    }
    getDirectory(responseHandler) {
        let ret = request.get(this.base.pathname)
                         .use(this.prefix);
        if (typeof responseHandler === 'function') {
            ret.end((error, response) => {
                if (response.headers['replay-nonce'])
                   this.nonces.push(response.headers['replay-nonce']);
                if (!error)
                   this.directory = response.body;
                responseHandler(error, this.directory);
            });
        }
        else {
            return ret.then((response) => {
                if (response.headers['replay-nonce'])
                   this.nonces.push(response.headers['replay-nonce']);
                this.directory = response.body;
                return response.body;
            });
        }
    }
    newNonce(responseHandler) {
        let uri = new url.URL(this.directory.newNonce);
        let req = request.head(uri.pathname)
                         .use(this.prefix);
        if (typeof responseHandler === 'function') {
            req.end((error, response) => {
                if (response?.headers['replay-nonce'])
                    this.nonces.push(response.headers['replay-nonce']);
                responseHandler(error, response);
            });
        }
        else {
            return req.then((response) => {
                this.nonces.push(response.headers['replay-nonce']);
            });
        }
    }
    async sendMessage(uri, message, responseHandler) {
        //logger.debug(`POST ${(new url.URL(uri).pathname)}`);
        //logger.debug(`sendMessage: ${inspect(message, undefined, 4)}`);
        let req = request.post(new url.URL(uri).pathname)
                         .use(this.prefix)
                         .set('Content-Type', 'application/json')
                         .send(JSON.stringify(message))
                         .buffer();
        if (typeof responseHandler === 'function') {
            req.end((error, response) => {
                //logger.verbose(`got response: ${inspect(response.body)}`);
                //logger.debug(`nonce: ${response.header['replay-nonce']}`);
                if (response?.headers['replay-nonce'])
                    this.nonces.push(response.headers['replay-nonce']);
                //logger.debug(`response.body: ${inspect(response.body)}`);
                responseHandler(error, response);
            });
        }
        else {
            return new Promise((resolve, reject) => {
                req.end((error, response) => {
                    if (response?.headers['replay-nonce'])
                        this.nonces.push(response.headers['replay-nonce']);
                    if (error)
                        reject(error);
                    else
                        resolve(response);
                });
            });
        }
    }
    request(uri, key, payload, responseHandler) {
        //logger.debug(`request payload: ${inspect(payload)}`);
        let message = {
            'protected': {
                alg: key.alg,
                jwk: key.jwk,
                nonce: this.nonces.pop(),
                url: `${this.base.protocol}//${this.base.hostname}:${this.base.port}${uri.pathname}`
            },
            'payload': payload
        };
        //logger.debug(`request message: ${inspect(message)}`);
        let signed = signMessage(key, message);
        //logger.debug(`request signed: ${inspect(signed)}`);
        return this.sendMessage(message.protected.url, signed, responseHandler);
    }
    createKey(key) {
        if (key === undefined) {
            key = crypto.generateKeyPairSync('ec', {namedCurve: 'prime256v1'});
            key.alg = 'ES256';
        }
        //key.pkcs8 = key.privateKey.export({type: 'pkcs8', format: 'pem'});
        key.jwk = key.publicKey.export({type: 'pkcs1', format: 'jwk'});
        return key;
    }
    async newAccount(key, payload, responseHandler) {
        if (this.directory === undefined)
            this.directory = await this.getDirectory();

        //logger.debug(`newAccount payload: ${inspect(payload)}`);
        let uri = new url.URL(this.directory.newAccount);
        let _payload = {
            termsOfServiceAgreed: true,
            contact: []
        };
        //logger.debug(`newAccount active: ${inspect(payload ?? _payload)}`);
        if (typeof responseHandler === 'function') {
            this.request(uri, key, payload ?? _payload, (error, response) => {
                let account = new Account(this, response.headers['location'], key);
                responseHandler(error, account);
            });
        }
        else {
            return new Promise((resolve, reject) => {
                this.request(uri, key, payload ?? _payload, (error, response) => {
                    if (error) {
                        reject(error);
                    }
                    else {
                        let account = new Account(this, response.headers['location'], key);
                        resolve(account);
                    }
                });
            });
        }
    }
    async recoverAccount(key, responseHandler) {
        return this.newAccount(key, {'onlyReturnExisting': true}, responseHandler);
    }
    async revokeCert(key, cert, reason, responseHandler) {
        let uri = new url.URL(this.directory.revokeCert);
        let pem = cert.toString();
        let b64 = pem.replaceAll(/-{5}(BEGIN|END) CERTIFICATE-{5}[\n\r]?/g, '');
        let b64u = encode.b64tob64u(b64);
        let payload = {'certificate': b64u, 'reason': reason};
        if (typeof responseHandler === 'function') {
            this.request(uri, key, payload, responseHandler);
        }
        else {
            return new Promise((resolve, reject) => {
                this.request(uri, key, payload, (error, response) => {
                    if (error)
                        reject(error);
                    else
                        resolve(response.body);
                });
            });
        }
    }
}

class Account {
    constructor(client, kid, key, responders) {
        this.client = client;
        this.kid = new url.URL(kid);
        this.key = key;
        this.responders = responders;
    }
    static async create(uri, responders, key) {
        let client = new Client(new url.URL(uri));
        let acmeKey = client.createKey(key);
        let account = await client.newAccount(acmeKey);
        account.responders = responders;
        return account;
    }
    request(uri, payload, responseHandler) {
        //logger.debug(`request payload: ${inspect(payload)}`);
        let message = {
            'protected': {
                alg: this.key.alg,
                kid: this.kid,
                nonce: this.client.nonces.pop(),
                url: `${this.client.base.protocol}//${this.client.base.hostname}:${this.client.base.port}${uri.pathname}`
            },
            'payload': payload
        };
        //logger.debug(`request message: ${inspect(message)}`);
        let signed = signMessage(this.key, message);
        //logger.debug(`request signed: ${inspect(signed)}`);
        return this.client.sendMessage(message.protected.url, signed, responseHandler);
    }
    sendPostAsGet(uri, responseHandler) {
        uri = new url.URL(uri);
        if (typeof responseHandler === 'function') {
            this.request(uri, {}, (error, response) => {
                responseHandler(error, response?.body);
            });
        }
        else {
            return this.request(uri, {})
                       .then((response) => {
                           return response.body;
                       });
        }
    }
    deactivate(responseHandler) {
        let message = {status: "deactivated"};
        if (typeof responseHandler === 'function') {
            this.request(this.kid, message, (error, response) => {
                this.status = response.body.status;
                this.orders = response.body.orders;

                responseHandler(error, this);
            });
        }
        else {
            return this.request(this.kid, message)
                       .then((response) => {
                           this.status = response.body.status;
                           this.orders = response.body.orders;
                           return this;
                       });
        }
    }
    newAuthz(identifier, responseHandler) {
        let uri = new url.URL(this.client.directory.newAuthz);
        let message = { identifier,
                        // notBefore: new Date().toISOString(),
                        notAfter: new Date(Date.now() + 365 * 24 * 60 * 60 * 1000).toISOString() };
        if (typeof responseHandler === 'function') {
            this.request(uri, message, (error, response) => {
                             let authorization = response.body;
                             authorization.url = response.headers['location'];
                             responseHandler(error, authorization);
                         });
        }
        else {
            return this.request(uri, message)
                       .then((response) => {
                           let authorization = response.body;
                           authorization.url = response.headers['location'];
                           return authorization;
                       });
        }
    }
    newOrder(identifiers, responseHandler) {
        let message = { identifiers,
                        // notBefore: new Date().toISOString(),
                        notAfter: new Date(Date.now() + 365 * 24 * 60 * 60 * 1000).toISOString() };
        return this._newOrder(message, responseHandler);
    }
    newStarOrder(identifiers, renewal, responseHandler) {
        const _renewal = {
            'start-date': new Date().toISOString(),
            'end-date': new Date(Date.now() + 30 * 24 * 60 *60 * 1000).toISOString(),
            lifetime: 60 * 60 * 24
        };
        let message = {identifiers, 'auto-renewal': renewal ?? _renewal};
        return this._newOrder(message, responseHandler);
    }
    _newOrder(message, responseHandler) {
        let uri = new url.URL(this.client.directory.newOrder);
        if (typeof responseHandler === 'function') {
            this.request(uri, message, (error, response) => {
                             let order = response.body;
                             order.url = response.headers['location'];
                             responseHandler(error, order);
                         });
        }
        else {
            return this.request(uri, message)
                       .then((response) => {
                           let order = response.body;
                           order.url = response.headers['location'];
                           return order;
                       });
        }
    }
    cancelOrder(order, responseHandler) {
        let uri = new url.URL(order.url);
        const message = {status: 'canceled'};
        if (typeof responseHandler === 'function') {
            this.request(uri, message, (error, response) => {
                let order = response.body;
                order.url = response.headers['location'];
                responseHandler(error, order);
            });
        }
        else {
            return this.request(uri, message)
                       .then((response) => {
                           let order = response.body;
                           order.url = response.headers['location'];
                           return order;
                       });
        }
    }
    finalizeOrder(order, csr, attestation, responseHandler) {
        const pem = csr.replaceAll(/-{5}(BEGIN|END) CERTIFICATE REQUEST-{5}[\n\r]?/g, '');
        let payload = {
           csr: encode.b64tob64u(pem)
        };
        if (attestation?.length > 0) {
            payload.attestation = [];
            for (let cert of attestation) {
                let b64 = cert.replaceAll(/-{5}(BEGIN|END) CERTIFICATE-{5}[\n\r]?/g, '');
                let b64u = encode.b64tob64u(b64);
                payload.attestation.push(b64u);
            }
        }
        let uri = new url.URL(order.finalize);
        if (typeof responseHandler === 'function') {
            this.request(uri, payload, (error, response) => {
                responseHandler(error, response?.body);
            });
        }
        else {
            return this.request(uri, payload)
                       .then((response) => {
                           return response?.body;
                       });
        }
    }
    retrieveCertificate(order, responseHandler) {
        let certificateUrl = order.certificate ?? order['star-certificate'];
        let uri = new url.URL(certificateUrl);

        this.request(uri, {}, (error, response) => {
            let certs = [];
            if (error) {
                responseHandler(error);
            }
            else {
                //logger.debug(`got response: ${inspect(response)}`);
                var candidates = response.text.toString('binary').split(
                    /(?<=-{5}END CERTIFICATE-{5})[\n\r]+/gm);
                for (let candidate of candidates) {
                    try {
                        let cert = new crypto.X509Certificate(candidate);
                        certs.push(cert);
                    }
                    catch { /* nothing */ }
                }
                responseHandler(undefined, certs);
            }
        });
    }
    revokeCertificate(certificate, reason = 4, responseHandler) {
        let b64 = certificate.replace(/^-{5}.*$/m, '');
        let payload = {
            reason: reason,
            certificate: encode.b64tob64u(b64)
        }
        let uri = new url.URL(this.client.directory.revokeCert);
        return this.request(uri, payload, responseHandler);
    }
    async solve(authorization, challenge, resultHandler) {
        for (let responder of this.responders) {
            if ((typeof responder.type === 'string' && responder.type == challenge.type)
              || (responder.type.includes(challenge.type))) {
                try {
                    await responder.solve(this.key, authorization, challenge);
                }
                catch (error) {
                    logger.error(`responder failed: ${error}`);
                }
                break;
            }
        }
        try {
            await this.pollChallenge(challenge, resultHandler);
        }
        catch (error) {
            logger.error(`querying challenge failed: ${error}`);
        }
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    async poll(ms, count, check) {
        let iterations;
        for (iterations=0; iterations<count && ! await check(iterations); iterations++) {
            await this.sleep(ms);
        }
        if (iterations>=count)
            throw new Error(`Timed out after ${iterations} tries`);
    }

    async pollChallenge(challenge, responseHandler) {
        await this.poll(14 * 1000, 24, async (counter) => {
            let response = await this.sendPostAsGet(challenge.url);
            //logger.debug(`challenge-${counter} ${response?.url} status: ${response?.status}`);
            switch (response?.status) {
                case 'valid':
                    responseHandler(undefined, response);
                    return true;
                case 'invalid':
                    responseHandler(new Error('Challenge is invalid'));
                    return true;
                case 'pending':
                default:
                    break;
            }
            return false;
        });
    }

    getChallenge(challenge, responseHandler) {
        return this.sendPostAsGet(challenge.url, responseHandler);
    }
    async solveOne(authorization, challenges) {
        var solvers = [];
        for (let responder of this.responders) {
            for (let challenge of authorization.challenges) {
                if ((typeof responder.type === 'string' && responder.type === challenge.type)
                  || (responder.type.includes(challenge.type))) {
                    solvers.push(new Promise((resolve, reject) => {
                        try {
                            responder.solve(this.key, authorization, challenge);
                            logger.debug(`setup challenge with ${responder.constructor.name}`);
                            this.sendPostAsGet(challenge.url, (error, challenge) => {
                                if (error) {
                                    logger.error(`challenge failed`);
                                    reject(error);
                                }
                                else {
                                    resolve(challenge);
                                }
                            });
                        }
                        catch (error) {
                            reject(new Error(`challenge cannot be solved (${error})`));
                        }
                    }));
                    authorization.handled = true;
                    break;
                }
            }
            if (authorization.handled) {
                logger.debug(`authorization for identifier {type: ${authorization.identifier.type}, value: ${authorization.identifier.value}} handled`);
                break;
            }
        }
        return Promise.all(solvers);
    }

    preAuthorize(identifier, responseHandler) {
        this.client.newNonce((error) => {
            if (error) throw error;
            this.newAuthz(identifier, (error, preAuthorization) => {
                if (error) {
                    responseHandler(error);
                }
                else {
                    //logger.debug(`preAuthorization: ${inspect(preAuthorization)}`);
                    this.solveOne(preAuthorization, preAuthorization.challenges)
                        .then((challenge) => {
                            //logger.debug(`solved all challenges: ${inspect(challenge)}`);
                            //logger.debug(`solved challenge: ${inspect(challenge)}`);
                            return this.sendPostAsGet(preAuthorization.url);
                        })
                        .then((authorization) => {
                            responseHandler(undefined, authorization);
                        })
                        .catch((error) => {
                            logger.debug(`got error solving challenges: ${inspect(error)}`);
                            responseHandler(error);
                        });
                }
            });
        });
    }
    getCertificate(identifiers, csr, attestation, responseHandler) {
        let orderUrl;
        this.client.newNonce()
            .then(() => {
                return this.newOrder(identifiers);
            })
            .then((order) => {
                orderUrl = order.url;
                return this.authorizeOrder(order);
            })
            .then((authorization) => {
                return this.sendPostAsGet(orderUrl);
            })
            .then((order) => {
                return this.finalizeOrder(order, csr, attestation);
            })
            .then((order) => {
                return this.retrieveCertificate(order, responseHandler);
            })
            .catch((error) => {
                logger.error(`cannot get certificate (${error})`);
                throw error;
            });
    }
    async authorizeOrder(order, responseHandler) {
        let authorizations = [];
        for (let authorizationUrl of order.authorizations) {
            authorizations.push(new Promise((resolve, reject) => {
                var solvers = [];
                solvers.push(new Promise((resolve, reject) => {
                    this.sendPostAsGet(authorizationUrl, (error, authorization) => {
                        authorization.url = authorizationUrl;
                        this.solveOne(authorization, authorization.challenges)
                            .then((challenge) => {
                                logger.debug(`solved challenge: ${inspect(challenge)}`);
                                this.sendPostAsGet(authorizationUrl, (error, authorization) => {
                                    if (error)
                                        reject(error);
                                    else
                                        resolve(authorization);
                                });
                            })
                            .catch((error) => {
                                logger.error(`got error solving challenges: ${inspect(error)}`);
                                reject(error);
                            });
                    });
                }));
                Promise.all(solvers)
                       .then((authorizations) => {
                           resolve(authorizations);
                       })
                       .catch((error) => {
                           reject(error);
                       });
            }));
        }
        if (typeof responseHandler === 'function') {
            Promise.all(authorizations)
               .then((authorizations) => {
                   responseHandler(undefined, authorizations);
               })
               .catch((error) => {
                   responseHandler(error);
               });
        }
        else {
            return Promise.all(authorizations);
        }
    }
}

export default {
    Account,
    Client,
    DnsProxyChallengeResponder,
    DnsmasqChallengeResponder,
    EmailChallengeResponder,
    ManualChallengeResponder,
    WebrootChallengeResponder
};
