import process from 'node:process';

import sqlite3 from 'sqlite3';

class Database {

    constructor(emitter, config) {
        this.emitter = emitter;
        this.config = config ?? {};

        logger.debug(`opening database '${this.config.file}' ...`);
        this.database = new sqlite3.Database(this.config.file, (error) => {
            if (error) throw error;
            let accounts = `CREATE TABLE IF NOT EXISTS
                               accounts (
                                   id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                   kid NVARCHAR(128) NOT NULL UNIQUE,
                                   key NVARCHAR(128) NOT NULL
                               );`;
            let acme_accounts = `CREATE TABLE IF NOT EXISTS
                                    acme_accounts (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                        account INTEGER NOT NULL,
                                        url NVARCHAR(512) NOT NULL,
                                        FOREIGN KEY (account) REFERENCES accounts (id)
                                    )`;
            let certificates = `CREATE TABLE IF NOT EXISTS
                                   certificates (
                                       id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                       acme_account INTEGER,
                                       certificate NVARCHAR(16384) NOT NULL,
                                       validFrom DATE NOT NULL,
                                       validTo DATE NOT NULL,
                                       serialNumber NVARCHAR(64) NOT NULL,
                                       issuer NVARCHAR(256) NOT NULL,
                                       UNIQUE(serialNumber, issuer),
                                       FOREIGN KEY (acme_account) REFERENCES acme_accounts (id)
                                   );`;
            let acme_contacts = `CREATE TABLE IF NOT EXISTS
                                    contacts (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                        acme_account INTEGER NOT NULL,
                                        email NVARCHAR(512) NOT NULL,
                                        FOREIGN KEY (acme_account) REFERENCES acme_accounts (id)
                                    );`;
            this.database.serialize(() => {
                this.database.run(accounts)
                        .run(acme_accounts)
                        .run(acme_contacts)
                        .run(certificates);
                this.database.each('SELECT COUNT(*) AS "count" FROM accounts;', [], (error, row) => {
                    if (error)
                        logger.error(`opening database failed (${error})`);
                    else
                        logger.info(`database opened, found ${row.count} account${row.count != 1 ? 's' : ''}`);
                });
            });
            this.emitter.on('certificate', (account, x509) => {
                const sql = `INSERT INTO 
                                certificates (
                                    acme_account,
                                    certificate,
                                    serialNumber,
                                    issuer,
                                    validFrom,
                                    validTo
                                )
                                VALUES
                                    (?, ?, ?, ?, ?, ?);`;
                 this.database.run(sql, [account?.id,
                                         x509.toString(),
                                         x509.serialNumber,
                                         x509.issuer.replace(/(^|\n)/g, '/'),
                                         x509.validFrom,
                                         x509.validTo], (error) => {
                     if (error)
                         logger.debug(`got SQL error ${error}`);
                 });
            });
            process.on('exit', (code) => {
                if (this.database) {
                    logger.debug(`got code ${code}, closing database ...`);
                }
            });
        });
    }
}

export default Database;
