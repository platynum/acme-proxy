import { Buffer } from 'node:buffer';

function b64tob64u(str) {
    return str.replace(/\+/g, '-')
              .replace(/\//g, '_')
              .replace(/=/g, '');
}

function b64utob64(str) {
    let tmp = str.replace(/-/g, '+')
                 .replace(/_/g, '/')
                 .replace(/=/g, '');
    if (tmp.length % 4 != 0)
        tmp += '=';
    if (tmp.length % 4 != 0)
        tmp += '=';
}

function b64utostr(b64) {
    return atob(b64utob64(b64));
}

function strtob64u(str) {
    let tmp =  Buffer.from(str, 'binary').toString('base64');
    return b64tob64u(tmp);
}

function b64toab(b64) {
    const str = Buffer.from(b64, 'base64').toString('binary');
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

export default { b64tob64u, b64utob64, b64utostr, strtob64u, b64toab };
