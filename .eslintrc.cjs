module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:unicorn/recommended"
    ],
    "parserOptions": {
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "plugins": [
        "unicorn"
    ],
    "rules": {
    },
    "globals": {
        "config": false,
        "logger": false
    }
};

