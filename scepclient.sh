#!/usr/bin/env bash
set -x

URL="https://$(hostname):3001/cgi-bin/pkiclient.exe"
export CONFDIR="$(pwd)/etc/ipsec.d"
mkdir -p "${CONFDIR}/"{cacerts,certs,reqs,private}

ipsec scepclient --out cacert="${CONFDIR}/cacerts/subCert.der" --url "${URL}" -f
openssl s_client -showcerts -connect "$(hostname):3001" </dev/null | openssl x509 -outform DER > "${CONFDIR}/cacerts/sslCert.der"

ipsec scepclient -f -l 1 \
       --method GET --url "${URL}" \
       --in cacert-enc="${CONFDIR}/cacerts/sslCert.der" \
       --in cacert-sig="${CONFDIR}/cacerts/sslCert.der" \
       --dn "CN=$(hostname)" -k 1024 -p $(openssl rand -hex 8) \
       -s dns=$(hostname) \
       --out pkcs1="${CONFDIR}/private/joeKey.der" \
       --out pkcs10="${CONFDIR}/reqs/joeKey.req" \
       --out cert="${CONFDIR}/certs/joeCert.der" \
       --out cert-self="${CONFDIR}/certs/joeCertSelf.der" \
       --out pkcs7="./joePkcs7.der"

