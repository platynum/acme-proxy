# ACMEv2 proxy

The ACMEv2 protocol can be proxied using this software. The proxy can take
over responsibility for a number of tasks, i.e.:

- translate incoming protocols like SCEP or EST into ACMEv2, or
- evaluate the external account binding (EAB) when creating new accounts, or
- publish certificates into an LDAP directory or external database, or
- notify the contacts provided by the user via email upon certificate
  generation/close expiry, or
- tigthen security with additional checks, or
- ...

This project serves as an example how this can be accomplished, YMMV. It
has been tested with https://gitlab.com/platynum/certification-authority/

## Pre-requisites

- Install node >=16.13 and npm >=8.5
- Setup configuration (see `etc/config.js`)
- Setup ACME CA

## Installation

Install the required packages for `acme-proxy`:

```
$ npm install
```

### OpenBSD

OpenBSD installs OpenSSL along with node:

```
# pkg_add node
# ln -s /usr/bin/c++ /usr/bin/g++
```

Build PKCS#11 engine for OpenSSL:

```
$ pkg_add git automake autoconf
$ git clone https://github.com/OpenSC/libp11.git
$ cd libp11
$ ./bootstrap
$ CFLAGS=-I/usr/local/include/eopenssl11 \
  LDFLAGS=-L/usr/local/lib/eopenssl11 \
  ./configure  --with-enginesdir=/usr/local/lib/eopenssl11/engines-1.1
```

Install PKCS#11 engine for OpenSSL:

```
$ doas cp ./src/.libs/pkcs11.so /usr/local/lib/eopenssl11/engine-1.1
```

## Startup

Start-up `acme-proxy`:

```
$ npm start
```
