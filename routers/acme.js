import { Buffer } from 'node:buffer';
import { TextEncoder, inspect } from 'node:util';
import url from 'node:url';
import fs from 'node:fs';
import crypto from 'node:crypto';
const subtle = crypto.webcrypto.subtle;

import superagent from 'superagent';
import express from 'express';
import sqlite3 from 'sqlite3';
import Ajv from 'ajv';
import addFormats from 'ajv-formats';
import asn1 from '@lapo/asn1js';

function relay(request, response, callback) {
    let server = new url.URL(`${config.source}`);
    superagent.post(`${config.target}${request.originalUrl}`)
              .set('Accept', 'application/json')
              .set('Host', `${server.host}`)
              .buffer(true)
              .send(request.body)
              .end((creq, cres) => {
                  if (cres) {
                      if (cres.headers['replay-nonce'])
                          response.set('Replay-Nonce', cres.headers['replay-nonce']);
                      if (cres.headers['location'])
                          response.set('Location', cres.headers['location']);
                      if (callback !== undefined)
                          callback(creq, cres);
                      else
                          response.status(cres.status)
                                  .send(cres.body);
                  }
                  else {
                      response.status(cres.status)
                              .send(Buffer.alloc(0));
                  }
              });
}

function compareJwk(left, right) {
    if (left.kty != right.kty) {
        logger.debug('kty missmatch');
        return false;
    }
    switch (left.kty) {
        case 'RSA':
            if (left.e != right.e) {
                logger.debug('e missmatch');
                return false;
            }
            if (left.n != right.n) {
                logger.debug('n missmatch');
                return false;
            }
            break;
         case 'EC':
            if (left.crv != right.crv) {
                logger.debug('crv missmatch');
                return false;
            }
            if (left.x != right.x) {
                logger.debug('x missmatch');
                return false;
            }
            if (left.y != right.y) {
                logger.debug('y missmatch');
                return false;
            }
            break;
        case 'OKP':
            if (left.crv != right.crv) {
                logger.debug('crv missmatch');
                return false;
            }
            if (left.x != right.x) {
                logger.debug('x missmatch');
                return false;
            }
            break;
        default:
            logger.debug(`unknown kty ${left.kty}`);
            return false;
    }
    return true;
}

function verifyEab(db, jwk, url, eab) {
    let eabPayloadBuf = Buffer.from(eab.payload, 'base64url');
    let eabPayload = JSON.parse(eabPayloadBuf);

    let eabProtectedBuf = Buffer.from(eab.protected, 'base64url');
    let eabProtected = JSON.parse(eabProtectedBuf);

    let eabSignatureBuf = Buffer.from(eab.signature, 'base64url');

    if (!compareJwk(eabPayload, jwk)) {
        logger.debug('key mismatch');
        return Promise.reject(new Error('key mismatch'));
    }
    if (eabProtected.nonce) {
        logger.debug('nonce must not be present');
        return Promise.reject(new Error('nonce must not be present'));
    }
    if (eabProtected.url !== url) {
        logger.debug('URL must be set to the same value as the outer JWS URL');
        return Promise.reject(new Error('URL must be set to the same value as the outer JWS URL'));
    }

    // logger.debug(`{kid: ${eabProtected.kid}, alg: ${eabProtected.alg}}`);
    let eabAlgorithm;
    switch (eabProtected.alg) {
        case 'HS256': eabAlgorithm = {name: 'HMAC', hash: {name: 'SHA-256'}}; break;
        case 'HS384': eabAlgorithm = {name: 'HMAC', hash: {name: 'SHA-384'}}; break;
        case 'HS512': eabAlgorithm = {name: 'HMAC', hash: {name: 'SHA-512'}}; break;
        default:
            logger.debug(`unsupported EAB algorithm ${eabProtected.alg}`);
            return Promise.reject(new Error(`unsupported EAB algorithm ${eabProtected.alg}`));
    }
    return new Promise((resolve, reject) => {
        db.all('SELECT id, key FROM accounts WHERE kid = ? AND key IS NOT NULL;', [eabProtected.kid], (error, rows) => {
            if (error || rows.length === 0) {
                logger.debug(`invalid KID (${eabProtected.kid})`);
                reject(new Error(`invalid KID (${eabProtected.kid})`));
            }
            else {
                let account = rows[0];
                logger.silly(`found account ${account.id} with key ${account.key}`);
                let eabKeyBuf = Buffer.from(account.key, 'base64url');
                subtle.importKey('raw', eabKeyBuf,
                                 eabAlgorithm, true, ['sign', 'verify'])
                      .then((key) => {
                          let tbs = `${eab.protected}.${eab.payload}`;
                          return subtle.verify(eabAlgorithm, key, eabSignatureBuf, new TextEncoder().encode(tbs));
                      })
                      .then((result) => {
                          // logger.debug(`EAB verify result: ${inspect(result)}`);
                          result ? resolve(account) : reject(new Error('cannot verify EAB signature'));
                      })
                      .catch((error) => {
                          logger.error(`Cannot verify EAB signature (got error: ${error})`);
                          reject(error);
                      });
            }
        });
    });
}

async function verifySignature(request) {
    let protectedBuf = Buffer.from(request.body.protected, 'base64url');
    let protectedJson = JSON.parse(protectedBuf.toString());

    let algorithm;
    switch (protectedJson.alg) {
        case 'RS256': algorithm = {name: 'RSASSA-PKCS1-v1_5',          hash: 'SHA-256'}; break;
        case 'RS384': algorithm = {name: 'RSASSA-PKCS1-v1_5',          hash: 'SHA-384'}; break;
        case 'RS512': algorithm = {name: 'RSASSA-PKCS1-v1_5',          hash: 'SHA-512'}; break;
        case 'PS256': algorithm = {name: 'RSA-PSS',                    hash: 'SHA-256'}; break;
        case 'PS384': algorithm = {name: 'RSA-PSS',                    hash: 'SHA-384'}; break;
        case 'PS512': algorithm = {name: 'RSA-PSS',                    hash: 'SHA-512'}; break;
        case 'ES256': algorithm = {name: 'ECDSA', namedCurve: 'P-256', hash: 'SHA-256'}; break;
        case 'ES384': algorithm = {name: 'ECDSA', namedCurve: 'P-384', hash: 'SHA-384'}; break;
        case 'ES512': algorithm = {name: 'ECDSA', namedCurve: 'P-521', hash: 'SHA-512'}; break;
        case 'EdDSA':
            switch (protectedJson.jwk.crv) {
                case 'Ed25519':
                      algorithm = {name: 'NODE-ED25519', hash: 'SHA-256'}; break;
                case 'X25519':
                      algorithm = {name: 'NODE-X25519',  hash: 'SHA-256'}; break;
                case 'Ed488':
                      algorithm = {name: 'NODE-ED448',   hash: 'SHA-512'}; break;
                case 'X488':
                      algorithm = {name: 'NODE-X448',    hash: 'SHA-512'}; break;
                default:
                      return Promise.reject(`unsupported curve ${protectedJson.jwk.crv}`);
            }
            break;
        default:      return Promise.reject(`unsupported algorithm ${protectedJson.alg}`);
    }
    // logger.debug(`protected: ${inspect(protectedJson)}`);
    // logger.debug(`algorithm: ${inspect(algorithm)}`);
    let key = await subtle.importKey('jwk', protectedJson.jwk, algorithm, true, ['verify'])
    logger.verbose(`key: ${inspect(key)}`);
    let sigBuf = Buffer.from(request.body.signature, 'base64url');
    let tbs = `${request.body.protected}.${request.body.payload}`;
    return await subtle.verify({name: algorithm.name, hash: {name: algorithm.hash}}, key,
                               sigBuf, new TextEncoder().encode(tbs));
}

class AcmeRouter {
    constructor(emitter, config) {
        this.emitter = emitter;
        this.delegations = [];

        this.db = new sqlite3.Database(config.database.file, (error) => {
            if (error) throw error;
        });

        if (config.acme.delegations) {
            const file = './data/rfc9115.schema';
            fs.readFile(file, (error, schema) => {
                if (error) {
                    logger.error(`cannot read schema ${file} (${error}), skipping validation ...`);
                }
                else {
                    const ajv = new Ajv();
                    addFormats(ajv);
                    const validate = ajv.compile(JSON.parse(schema));
                    fs.readdir(config.acme.delegations, {withFileTypes: true}, (error, files) => {
                        if (error) {
                            logger.error(`cannot read directory (${config.acme.delegations})`);
                        }
                        else {
                            for (let file of files) {
                                if (file.isFile()) {
                                    fs.readFile(`${config.acme.delegations}/${file.name}`, (error, data) => {
                                        if (error) {
                                            logger.error(`cannot read ${config.acme.delegations}/${file.name} (${error})`);
                                        }
                                        else {
                                            let delegation = JSON.parse(data);
                                            if (validate(delegation["csr-template"]))
                                                this.delegations.push({dns: file.name, delegation: delegation});
                                            else
                                                logger.error(`cannot validate ${file.name}`);
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            });
        }
        this.router = express.Router();
        this.router.param('ca', (request, response, next, id) => { 
            next(`${id}` != 'Sub' ? 'ca invalid' : undefined);
        });
        this.router.param('orderId', (request, response, next, id) => { 
            next(/^[\da-f]{64}$/.test(id) ? undefined : `orderId: ${id} invalid`);
        });
        this.router.param('authId', (request, response, next, id) => {
            next(/^[\da-f]{64}$/.test(id) ? undefined : `authId: ${id} invalid`);
        });
        this.router.param('challengeId', (request, response, next, id) => {
            next(/^[\da-f]{64}$/.test(id) ? undefined : `challengeId: ${id} invalid`);
        });
        this.router.param('accountId', (request, response, next, id) => {
            next(/^[\da-f]{64}$/.test(id) ? undefined : `accountId: ${id} invalid`);
        });
        this.router.param('delegationId', (request, response, next, id) => {
            next(/^[\da-f]{64}$/.test(id) ? undefined : `delegationId: ${id} invalid`);
        });

        this.router.get('/:ca/', (request, response) => {
            response.redirect(`${request.path}/directory`);
        });
        this.router.get('/:ca/directory', (request, response) => {
            let server = new url.URL(`${config.source}`);
            let delegations = (this.delegations.length > 0);
            superagent.get(`${config.target}${request.originalUrl}`)
                      .set('Accept', 'application/json')
                      .set('Host', `${server.host}`)
                      .buffer(true)
                      .end((creq, cres) => {
                          const actions = [
                              'newNonce',   'newAccount',
                              'newOrder',   'newAuthz',
                              'revokeCert', 'keyChange'];
                          let out = {};
                          for (let action of actions) {
                              let l = new url.URL(cres.body[action]);
                              out[action] = `${config.source}${l.pathname}`;
                          }

                          out.meta = cres.body.meta ?? {};
                          out.meta.externalAccountRequired = true;
                          if (delegations)
                              out.meta['delegation-enabled'] = true;

                          if (cres.headers['replay-nonce'])
                              response.set('Replay-Nonce', cres.headers['replay-nonce']);
                          if (cres.body['auto-renewal'])
                              out['auto-renewal'] = cres.body['auto-renewal'];

                          response.status(cres.status).send(out);
                      });
        });
        this.router.get('/:ca/newNonce', (request, response) => {
            superagent.get(`${config.target}${request.originalUrl}`)
                      .end((creq, cres) => {
                          if (cres.headers['replay-nonce'])
                              response.set('Replay-Nonce', cres.headers['replay-nonce']);
                          response.status(204).send(Buffer.alloc(0));
                      });
        });
        this.router.post('/:ca/newAccount', async (request, response) => {
            let payloadBuf = Buffer.from(request.body.payload, 'base64url');
            let payload = JSON.parse(payloadBuf.toString());
            // logger.debug(`payload: ${inspect(payload)}`);
            let delegations = (this.delegations.length > 0);

            if (!config.eab.required) {
                relay(request, response, (creq, cres) => {
                    if (cres.status === 201 || cres.status === 200) {
                        if (delegations) { /* Inject delegations */
                            let server = new url.URL(`${config.source}`);
                            cres.body.delegations = `https://${server.host}${request.baseUrl}/${request.params.ca}/delegations/:accountId`;
                        }
                    }
                    response.status(cres.status)
                            .send(cres.body);
                });
            }
            else {
                if (!payload.externalAccountBinding) {
                    response.status(401)
                            .send({
                                type: 'urn:ietf:params:acme:error:unauthorized',
                                detail: 'The client lacks sufficient authorization',
                                subproblems: [{
                                    type: 'urn:ietf:params:acme:error:malformed',
                                    detail: 'The proxy needs external account binding'
                                }]
                            });
                }
                else {
                    let verified = await verifySignature(request);
                    // logger.debug(`verified: ${verified}`);
                    if (!verified) {
                        response.status(401)
                                .send({
                                    type: 'urn:ietf:params:acme:error:unauthorized',
                                    detail: 'The client lacks sufficient authorization',
                                    subproblems: [{
                                        type: 'urn:ietf:params:acme:error:malformed',
                                        detail: 'The proxy cannot verify the signature'
                                    }]
                                });
                    }
                    else {
                        let protectedBuf = Buffer.from(request.body.protected, 'base64url');
                        let protectedJson = JSON.parse(protectedBuf.toString());

                        verifyEab(this.db, protectedJson.jwk, protectedJson.url, payload.externalAccountBinding)
                        .then((account) => {
                            relay(request, response, (creq, cres) => {
                                // logger.debug(`cres.body: ${inspect(cres.body)}`);
                                if (cres.status === 201) { // Created
                                    let url = cres.headers['location'];
                                    this.db.all('INSERT INTO acme_accounts(id, account, url) VALUES(NULL, ?, ?) RETURNING *;', [account.id, url], (error, rows) => {
                                        if (cres.body.contact && Array.isArray(cres.body.contact) && cres.body.contact.length > 0)
                                            this.db.run('INSERT INTO contacts (acme_account, email) VALUES (?, ?) ON CONFLICT DO NOTHING;', [rows[0].id, cres.body.contact])
                                    });
                                }
                                response.status(cres.status)
                                        .send(cres.body);
                            });
                        })
                        .catch((error) => {
                            logger.verbose(`got an error while verifying EAB: ${error}`);
                            response.status(401)
                                    .send({
                                        type: 'urn:ietf:params:acme:error:unauthorized',
                                        detail: 'The client lacks sufficient authorization',
                                        subproblems: [{
                                            type: 'urn:ietf:params:acme:error:unauthorized',
                                            detail: 'The proxy cannot verify the external account binding'
                                        }]
                                    });
                        });
                    }
                }    
            }
        });
        this.router.post('/:ca/account/:accountId', (request, response) => {
            let delegations = (this.delegations.length > 0);
            relay(request, response, (creq, cres) => {
                if (cres.status === 200 && cres.body.status === 'valid') {
                     let kid = `${config.source}/acme${request.path}`;
logger.error(`kid: ${kid}`);
                     this.db.serialize(() => {
                         this.db.run('INSERT INTO contacts (acme_account, email) VALUES ((SELECT id FROM acme_accounts WHERE url = ?), ?) ON CONFLICT DO NOTHING;', [kid, cres.body.contact]);
                         this.db.run('DELETE FROM contacts WHERE acme_account = (SELECT id FROM acme_accounts WHERE url = ?) AND email NOT IN (?);', [kid, cres.body.contact]);
                     });
                }
                if (delegations) { /* Inject delegations */
                    let server = new url.URL(`${config.source}`);
                    cres.body.delegations = `https://${server.host}${request.baseUrl}/:ca/delegations/${request.params.accountId}`;
                }
                response.status(cres.status)
                        .send(cres.body);
            });
        });
        this.router.post('/:ca/delegations/:accountId', (request, response) => {
            let delegations = (this.delegations.length > 0);
            let server = new url.URL(`${config.source}`);
            relay(request, response, (creq, cres) => {
                let list = (cres.status === 404) ? {delegations: []} : cres.body;
                for (let delegation of delegations)
                    list.delegations.push(`https://${server.host}${request.baseUrl}/${request.params.ca}/delegation/${request.params.accountId}/${delegation.id}`);
                response.status(200)
                        .send(list);
            });
        });
        this.router.post('/:ca/delegation/:accountId/:delegationId', (request, response) => {
            relay(request, response);
        });
        this.router.post('/:ca/authorization/:orderId/:authId', (request, response) => {
            relay(request, response);
        });
        this.router.post('/:ca/challenge/:orderId/:authId/:challengeId', (request, response) => {
            relay(request, response);
        });
        this.router.post('/:ca/order/:orderId', (request, response) => {
            relay(request, response);
        });
        this.router.post('/:ca/finalize/:orderId', (request, response) => {
            /* Mini WAF */
            try {
                let payload = Buffer.from(request.body.payload, 'base64url');
                let csr = payload.toString('binary');
                asn1.decode(csr);
                relay(request, response);
            }
            catch (error) {
                response.status(400).send({
                    'type': 'urn:ietf:params:acme:error:badCSR',
                    'detail': 'Parsing CSR failed'
                });
            }
        });
        this.router.post('/:ca/certificate/:orderId', (request, response) => {
            let server = new url.URL(`${config.source}`);
            let accept = request.headers['accept-encoding'] ?? 'application/pem-certificate-chain';
            superagent.post(`${config.target}${request.originalUrl}`)
                      .set('Host', `${server.host}`)
                      .set('Accept', `${accept}`)
                      .send(request.body)
                      .end((creq, cres) => {
                          /* Alternatives: application/pkix-cert or application/pkcs7-mime */
                          if (cres.headers['content-type'] === 'application/pem-certificate-chain') {
                              let protectedBuf = Buffer.from(request.body.protected, 'base64url');
                              let protectedJson = JSON.parse(protectedBuf.toString('binary'));
                              this.db.all('SELECT id FROM accounts WHERE url = ?;', [protectedJson.kid], (error, accounts) => {
                                  if (error || accounts.length === 0)
                                      throw `invalid kid (${protectedJson.kid})`;
                                  let account = accounts[0];
 
                                  let candidates = cres.body.toString('binary').split(
                                      /(?<=-{5}END CERTIFICATE-{5})[\n\r]+/gm);
                                  let cert = new crypto.X509Certificate(candidates[0]);
                                  this.emitter.emit('certificate', account, cert);
                              });
                          }
         
                          if (cres.headers['content-type'])
                              response.type(cres.headers['content-type']);
                          if (cres.headers['replay-nonce'])
                              response.set('Replay-Nonce', cres.headers['replay-nonce']);
                          response.status(cres.status).send(cres.body);
                      });
        });
        this.router.post('/:ca/:operation(newOrder|orders|newAuthz|revokeCert|keyChange)', (request, response) => {
            relay(request, response);
        });
    }
}

export default { AcmeRouter };

