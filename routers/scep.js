import { Buffer } from 'node:buffer';
import { inspect } from 'node:util';

import express from 'express';
import forge from 'node-forge';
import jsrsasign from 'jsrsasign';

jsrsasign.KJUR.asn1.cms.AttributeList = function(parameters) {
	var _Error = Error,
	_KJUR = jsrsasign.KJUR,
	_KJUR_asn1 = _KJUR.asn1,
	_DERSet = _KJUR_asn1.DERSet,
	_KJUR_asn1_cms = _KJUR_asn1.cms;

	_KJUR_asn1_cms.AttributeList.superclass.constructor.call(this);

	this.parameters = null;
	this.hTLV = null;

	this.setByParam = function(parameters) {
		this.parameters = parameters;
	};

	this.getEncodedHex = function() {
		var parameters = this.parameters;
		if (this.hTLV != null) return this.hTLV;

		var sortflag = true;
		if (parameters.sortflag != undefined) {
			sortflag = parameters.sortflag;
		}

		var a = [];
		for (let pAttribute of parameters.array) {
			var attributeName = pAttribute.attr;
			if (attributeName == "contentType") {
				a.push(new _KJUR_asn1_cms.ContentType(pAttribute));
			} else if (attributeName == "messageDigest") {
				a.push(new _KJUR_asn1_cms.MessageDigest(pAttribute));
			} else if (attributeName == "signingTime") {
				a.push(new _KJUR_asn1_cms.SigningTime(pAttribute));
			} else if (attributeName == "signingCertificate") {
				a.push(new _KJUR_asn1_cms.SigningCertificate(pAttribute));
			} else if (attributeName == "signingCertificateV2") {
				a.push(new _KJUR_asn1_cms.SigningCertificateV2(pAttribute));
			} else if (attributeName == "signaturePolicyIdentifier") {
				a.push(new _KJUR.asn1.cades.SignaturePolicyIdentifier(pAttribute));
			} else if (attributeName == "signatureTimeStamp" ||
				attributeName == "timeStampToken") {
				a.push(new _KJUR.asn1.cades.SignatureTimeStamp(pAttribute));
			} else if (pAttribute.xattr) {
				if (pAttribute.oid == undefined || pAttribute.value == undefined)
					throw new _Error("missing oid or value attribute");
				a.push(jsrsasign.KJUR.asn1.ASN1Util.newObject({seq: [{"oid": pAttribute.oid}, pAttribute.value]}));
			} else {
				throw new _Error("unknown attr: " + attributeName);
			}
		}
	
		var dSet = new _DERSet({array: a, sortflag: sortflag});
		this.hTLV = dSet.getEncodedHex();
		return this.hTLV;
	};

	if (parameters != undefined) this.setByParam(parameters);
}
function extendClass(c, a) {
	var b = function() {};
	b.prototype = a.prototype;
	c.prototype = new b();
	c.prototype.constructor = c;
	c.superclass = a.prototype;
	if (a.prototype.constructor == Object.prototype.constructor) {
		a.prototype.constructor = a
	}
}
extendClass(jsrsasign.KJUR.asn1.cms.AttributeList, jsrsasign.KJUR.asn1.ASN1Object);

class Scep {

	/* Based on the work of NuSkooler found here:
	 *
	 * https://stackoverflow.com/questions/37310096/ios-mdm-scep-pkioperation-the-scep-server-returned-an-invalid-response
	 */
	async PKIOperation(request, response) {
		//
		//  |req.query.message| should contain a Base64 encoded PKCS#7 package.
		//  The SignedData portion is PKCS#7 EnvelopedData encrypted with the CA
		//  public key we gave the client in GetCACert. Once decrypted, we have
		//  ourselves the client's CSR.
		//
		try {

			let messageBuffer;
			if (request.method === 'POST') {
				messageBuffer = Buffer.from(request.body, 'base64');
			}
			else {
				if (!request.query.message) {
					return response.status(403)
										.send('The CA could not validate the request');
				}
				messageBuffer = Buffer.from(request.query.message, 'base64');
			}
			const p7Message = forge.pkcs7.messageFromAsn1(
				forge.asn1.fromDer(
					forge.util.createBuffer(messageBuffer, 'binary')
				)
			);
			//logger.verbose(`p7Message: ${JSON.stringify(p7Message)}`);

			//  :TODO: Validate integrity
			//  :TODO: Validated signing

			//
			//  The outter PKCS#7 signed data must contain authenticated
			//  attributes for transactionID and senderNonce. We will use these
			//  in our reply back as part of the SCEP spec.
			//
			let origTransactionId = p7Message.rawCapture.authenticatedAttributes.find( attribute => {
				const oid = forge.asn1.derToOid(attribute.value[0].value);
				return ('2.16.840.1.113733.1.9.7' === oid); //  transactionID
			});
			logger.silly(`origTransactionId: ${origTransactionId}`);

			if(!origTransactionId) {
				return response.status(403)
									.send('Invalid request payload');
			}
	
			origTransactionId = origTransactionId.value[1].value[0].value;  //  PrintableString
			logger.verbose(`origTransactionId: ${origTransactionId}`);

			let origSenderNonce = p7Message.rawCapture.authenticatedAttributes.find( attribute => {
				const oid = forge.asn1.derToOid(attribute.value[0].value);
				return ('2.16.840.1.113733.1.9.5' === oid); //  senderNonce
			});
			logger.silly(`origSenderNonce: ${origSenderNonce}`);

			if(!origSenderNonce) {
				return response.status(403)
									.send('Invalid request payload');
			}

			origSenderNonce = origSenderNonce.value[1].value[0].value;  //  OctetString
			logger.verbose(`origSenderNonce: ${Buffer.from(origSenderNonce, 'binary').toString('hex')}`);

			const p7EnvelopedData = forge.pkcs7.messageFromAsn1(
				forge.asn1.fromDer(
					forge.util.createBuffer(Buffer.from(p7Message.rawCapture.content.value[0].value, 'binary'), 'binary')
				)
			);

			//  decrypt using our key
			let privKey = forge.pki.privateKeyFromPem(this.key);
			p7EnvelopedData.decrypt(p7EnvelopedData.recipients[0], privKey);

			//  p7EnvelopedData should contain a PKCS#10 CSR
			const csrDataBuffer = Buffer.from(p7EnvelopedData.content.getBytes(), 'binary');
			const certs = await this.certify('-----BEGIN CERTIFICATE REQUEST-----\r\n'
											+ csrDataBuffer.toString('base64')
											+ '\r\n-----END CERTIFICATE REQUEST-----');
			logger.verbose(`certificate: ${certs[0]}`);

			const degenerate = forge.pkcs7.createSignedData();
			for (let cert of certs) {
				degenerate.addCertificate(cert);
			}
			degenerate.addCertificate(certs[0]);
			degenerate.sign();

			const enveloped = forge.pkcs7.createEnvelopedData();

			//  Recipient is the original requester cert
			enveloped.addRecipient(p7Message.certificates[0]);				  
			enveloped.content					   = forge.asn1.toDer(degenerate.toAsn1());
			enveloped.encryptedContent.algorithm	= forge.pki.oids['des-EDE3-CBC'];   //  We set this in GetCACaps
			enveloped.encrypt();
			let content = forge.asn1.toDer(enveloped.toAsn1()).getBytes();
	
			let sd = new jsrsasign.KJUR.asn1.cms.SignedData({
				version: 1,
				hashalgs: ["sha1"],
				econtent: {
					type: "data",
					content: { hex: Buffer.from(content, 'binary').toString('hex') }
				},
				certs: [
					certs[0],
					certs[1]
				],
				revinfos: {array: []},
				sinfos: [{
					version: 1,
					id: {type: 'isssn', cert: this.chain[0]},
					hashalg: 'sha1',
					sattrs: {array: [
						{attr: 'contentType',   type: '1.2.840.113549.1.7.1'},
						{attr: 'messageDigest', hex: 'abcd'},
						{xattr: 'transactionID', oid: '2.16.840.1.113733.1.9.7',
							value: {set: [{prnstr: origTransactionId}]} },
						{xattr: 'pkiStatus', oid: '2.16.840.1.113733.1.9.3',
							value: {set: [{prnstr: '0'}]} }, // SUCCESS
						{xattr: 'messageType', oid: '2.16.840.1.113733.1.9.2',
							value: {set: [{prnstr: '3'}]} }, // CertRep
						{xattr: 'senderNonce', oid: '2.16.840.1.113733.1.9.5',
							value: {set: [{octstr: forge.random.getBytes(16)}]} },
						{xattr: 'recipientNonce', oid: '2.16.840.1.113733.1.9.6',
							value: {set: [{octstr: origSenderNonce }]} }
					]},
					sigalg: 'SHA1with' + (this.key.type === 'EC' ? 'ECDSA' : 'RSA'),
					signkey: this.key
				}]
			});
			return response.type('application/x-pki-message')
									.status(200)
									.send(Buffer.from(sd.getContentInfoEncodedHex(), 'hex'));
		}
		catch (error) {
			logger.error(error);
			logger.error(`${inspect(error.stack)}`);
			return response.status(403)
								.send('The CA could not validate the request');
		}	   
	}

	GetCACaps(request, response) {
		response.status(200)
				.setHeader('Content-Type', 'text/plain')
				.send("AES\r\n" +
						"GetNextCACert\r\n" +
						"POSTPKIOperation\r\n" +
						"SCEPStandard\r\n" +
						"SHA-256\r\n");
	}

	GetCACert(request, response) {
		let b64, der;
		switch (this.chain.length) {
			case 1:
				b64 = this.chain[0].replace(/(-{5}(BEGIN|END) CERTIFICATE-{5}|[\n\r])/g, '');
				der = Buffer(b64, 'base64');
				response.setHeader('Content-Type', 'application/x-x509-ca-cert')
				break;
			case 2: {
				let sd = new jsrsasign.KJUR.asn1.cms.SignedData({
					version: 1,
					hashalgs: ["sha256"],
					econtent: {
						type: "data",
						content: { hex: "" }
					},
					certs: [
						this.chain[0],
						this.chain[1]
					],
					revinfos: {array: []},
					sinfos: [{
						version: 1,
						id: {type: 'isssn', cert: this.chain[0]},
						hashalg: 'sha256',
						sattrs: {array: [
							{attr: 'contentType',   type: '1.2.840.113549.1.7.1'},
							{attr: 'messageDigest', hex: 'abcd'}
						]},
						sigalg: 'SHA256with' + (this.key.type === 'EC' ? 'ECDSA' : 'RSA'),
						signkey: this.key
					}]
				});
				der = Buffer.from(sd.getContentInfoEncodedHex(), 'hex');
				response.setHeader('Content-Type', 'application/x-x509-ca-ra-cert');
				break;
			}
			default:
				response.status(500).send(Buffer.alloc(0));
				return;
		}
		response.status(200)
				.send(der);
	}

	constructor(key, chain, certify) {
		this.key = key;
		this.chain = chain;
		this.certify = certify;

		this.router = express.Router();

		this.router.get('/pkiclient.exe', (request, response) => {
			logger.info(`request.query: ${inspect(request.query)}`);
			switch (request.query.operation) {
				case 'GetCACaps':
					return this.GetCACaps(request, response);
				case 'GetCACert':
					return this.GetCACert(request, response);
				case 'PKIOperation':
					return this.PKIOperation(request, response);
				default:
					response.status(400)
							.send(Buffer.alloc(0));
					break;
			}
		});

		this.router.post('/pkiclient.exe', (request, response) => {
			logger.info(`request.query: ${inspect(request.query)}`);
			switch (request.query.operation) {
				case 'PKIOperation':
					return this.PKIOperation(request, response);
				default:
					response.status(400)
							.send(Buffer.alloc(0));
					break;
			}
		});
	}
}

export default { Scep };

